#!/home/hz/.rbenv/shims/ruby

def merge_sort(list)
  puts list
  if list.length < 2
    list
  else
    semi_sort = []
    left = merge_sort(list[0..list.length/2])
    right = merge_sort(list[list.length/2..-1])
    until left.empty? || right.empty?
      left[0] < right[0] ? semi_sorted << left.shift : semi_sorted << right.shift
    end
    semi_sorted + (left + right)
  end
end

list = [1,2,3,4,5,6,7,8]
puts merge_sort(list)
